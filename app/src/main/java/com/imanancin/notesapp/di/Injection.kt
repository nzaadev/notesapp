package com.imanancin.notesapp.di

import android.content.Context
import com.imanancin.notesapp.data.AppDatabase
import com.imanancin.notesapp.data.NoteRepository

object Injection {

    fun provideDatabase(applicationContext: Context): AppDatabase {
        return AppDatabase.getDatabase(applicationContext)
    }

    fun provideNoteRepository(context: Context): NoteRepository {
        return NoteRepository(provideDatabase(applicationContext = context))
    }
}