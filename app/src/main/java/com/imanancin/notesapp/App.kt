package com.imanancin.notesapp

import android.app.Application
import com.imanancin.notesapp.data.AppDatabase
import com.imanancin.notesapp.di.Injection

class App: Application() {

    lateinit var appDatabase: AppDatabase

    override fun onCreate() {
        super.onCreate()
        appDatabase = Injection.provideDatabase(applicationContext)
    }
}