package com.imanancin.notesapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imanancin.notesapp.R
import com.imanancin.notesapp.ViewModelFactory
import com.imanancin.notesapp.domain.MyNote



class AddFragment : Fragment() {

    private val TAG = "AddFragment"
    private val viewModel by activityViewModels<MyViewModel> { ViewModelFactory() }


    

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnSave = view.findViewById<ImageButton>(R.id.btnSave)
        val edTitle = view.findViewById<EditText>(R.id.edTitle)
        val edContent = view.findViewById<EditText>(R.id.edContent)

        btnSave.setOnClickListener {
            viewModel.addNote(
                MyNote(null, edTitle.text.toString(), edContent.text.toString())
            )
            findNavController().popBackStack()
        }
    }

}