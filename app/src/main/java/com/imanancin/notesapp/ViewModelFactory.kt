package com.imanancin.notesapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewmodel.CreationExtras
import com.imanancin.notesapp.data.NoteRepository
import com.imanancin.notesapp.ui.MyViewModel

class ViewModelFactory: ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras) = when(modelClass) {
        MyViewModel::class.java -> {
            val application = checkNotNull(extras[APPLICATION_KEY])
            MyViewModel(NoteRepository((application as App).appDatabase))
        }
        else -> throw IllegalArgumentException("Unknown class model")
    } as T

}