package com.imanancin.notesapp.domain

data class MyNote(
    val uid: Int?,
    val title: String,
    val content: String,
)
