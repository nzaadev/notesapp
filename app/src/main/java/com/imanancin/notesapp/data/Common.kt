package com.imanancin.notesapp.data

import com.imanancin.notesapp.domain.MyNote

object Common {

    fun dataMappingNote(note: MyNote): Note {
        return Note(
            uid = note.uid,
            title = note.title,
            content = note.content
        )
    }

    fun dataMappingNoteEntity(note: Note) : MyNote {
        return MyNote(
            note.uid!!,
            note.title!!,
            note.content!!
        )
    }

}