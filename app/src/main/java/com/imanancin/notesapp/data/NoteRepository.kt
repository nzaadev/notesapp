package com.imanancin.notesapp.data

import com.imanancin.notesapp.domain.MyNote
import kotlinx.coroutines.flow.map

class NoteRepository(
    appDatabase: AppDatabase
) {

    private val dao = appDatabase.noteDao()

    suspend fun addNote(note: MyNote) {
        dao.insertAll(Common.dataMappingNote(note))
    }

    fun getNote(id: Int) = dao.getNote(id)

    suspend fun deleteNote(noteId: Int) = dao.delete(noteId)
    suspend fun updateNote(note: MyNote) = dao.updateNote(Common.dataMappingNote(note))

    fun getNotes() = dao.getAll().map {
        it.map { note ->
            Common.dataMappingNoteEntity(note)
        }
    }


}