package com.imanancin.notesapp

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {

    private lateinit var sp: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sp = getSharedPreferences("settings", MODE_PRIVATE)

        loadDarkModeSetting()
    }

    fun getDarkMode(): Boolean {
        return sp.getBoolean("dark_mode", false)
    }

    fun getViewMode(): Boolean {
        return sp.getBoolean("view_mode", false)
    }

    fun setViewMode(boolean: Boolean) {
        edit = sp.edit()
        edit.putBoolean("view_mode", boolean)
        edit.apply()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun loadDarkModeSetting() {
        if (getDarkMode()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    fun setDarkMode(darkMode: Boolean) {
        edit = sp.edit()
        edit.putBoolean("dark_mode", darkMode)
        edit.apply()
        if (darkMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}