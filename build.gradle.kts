// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    dependencies {
        val nav_version = "2.7.6"
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$nav_version")
    }
}

plugins {
    id("com.android.application") version "8.1.2" apply false
    id("org.jetbrains.kotlin.android") version "1.9.21" apply false
    // ksp https://github.com/google/ksp/issues/1481#issuecomment-1784020188
    id("com.google.devtools.ksp") version "1.9.21-1.0.15" apply false
}