package com.imanancin.notesapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imanancin.notesapp.data.NoteRepository
import com.imanancin.notesapp.domain.MyNote
import kotlinx.coroutines.launch

class MyViewModel(
    val noteRepository: NoteRepository
): ViewModel() {

    private val TAG = "MyViewModel"

    fun getNotes() = noteRepository.getNotes()

    fun getNote(id: Int) = noteRepository.getNote(id)

    suspend fun updateNote(myNote: MyNote) = noteRepository.updateNote(myNote)
    suspend fun deleteNote(idNote: Int) = noteRepository.deleteNote(idNote)

    fun addNote(note: MyNote) {
        viewModelScope.launch {
            noteRepository.addNote(note)
        }
    }

}