package com.imanancin.notesapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.imanancin.notesapp.domain.MyNote

class NoteAdapter(
    val onClick: (note: MyNote) -> Unit
): RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    val diff = object : DiffUtil.ItemCallback<MyNote>() {
        override fun areItemsTheSame(oldItem: MyNote, newItem: MyNote): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: MyNote, newItem: MyNote): Boolean {
            return oldItem == newItem
        }

    };

    val asynclist = AsyncListDiffer(this, diff)

    class ViewHolder(view: View, onClick: (pos: Int) -> Unit) : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onClick(adapterPosition)
            }
        }

        var lastPosition  = -1

        private val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        private val tvContent = view.findViewById<TextView>(R.id.tvContent)
        fun bind(note: MyNote) {
            tvTitle.text = note.title
            tvContent.text = note.content
//            setAnimation(itemView,  position)
        }

//        fun setAnimation(view: View, position: Int) {
//            if(position > -1) {
                // Animation
//                val anim = AnimationUtils.loadAnimation(view.context, android.R.anim.slide_in_left)
//                view.startAnimation(anim)

//                val transX = ObjectAnimator.ofFloat(view, "translationX", -100f,0f)
//                transX.duration = 1000
//                val alpha = ObjectAnimator.ofFloat(view, "alpha", 0f,1f)
//                alpha.duration = 2000
//                AnimatorSet().apply {
//                    playTogether( alpha)
//                    start()
//                }
//
//                lastPosition = position
//            }
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return ViewHolder(view) {
            onClick(asynclist.currentList[it])
        }
    }

    override fun getItemCount(): Int = asynclist.currentList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(asynclist.currentList[position])
        holder.setIsRecyclable(false)
    }

}