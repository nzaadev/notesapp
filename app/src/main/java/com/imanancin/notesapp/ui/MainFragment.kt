package com.imanancin.notesapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.imanancin.notesapp.MainActivity
import com.imanancin.notesapp.NoteAdapter
import com.imanancin.notesapp.R
import com.imanancin.notesapp.ViewModelFactory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class MainFragment : Fragment() {


    private val TAG = "MainFragment"
    private lateinit var addNoteFab: FloatingActionButton
    private lateinit var rvNote: RecyclerView
    private lateinit var adapters: NoteAdapter
    private val viewModel by activityViewModels<MyViewModel> { ViewModelFactory() }
    private lateinit var toolbar: Toolbar
    private lateinit var activity: MainActivity
    lateinit var noData: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_main, container, false)
        toolbar = view.findViewById(R.id.toolbar)
        noData = view.findViewById(R.id.noData)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        (requireActivity() as MenuHost).addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.main_menu, menu)
            }

            override fun onPrepareMenu(menu: Menu) {
                val menuItem = menu.findItem(R.id.darkMode)
                val menuItemView = menu.findItem(R.id.viewMode)
                if(menuItem != null) {
                    menuItem.isChecked = activity.getDarkMode()
                }
                if(menuItemView != null) {
                    menuItemView.isChecked = activity.getViewMode()
                }
                super.onPrepareMenu(menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.darkMode -> {
                        val newVal = !activity.getDarkMode()
                        menuItem.isChecked = newVal
                        activity.setDarkMode(newVal)
                        return true
                    }
                    R.id.viewMode -> {
                        val newVal = !activity.getViewMode()
                        menuItem.isChecked = newVal
                        activity.setViewMode(newVal)
                        return true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)

        addNoteFab =  view.findViewById(R.id.addNote)
        rvNote = view.findViewById(R.id.rvNote)
        adapters = NoteAdapter {
            //  click details
            val bundle = bundleOf("noteid" to it.uid!!)
            findNavController().navigate(R.id.action_mainFragment_to_showFragment, bundle)
        }
        rvNote.apply {
            adapter = adapters
            layoutManager = if (activity.getViewMode())
                StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
            else
                LinearLayoutManager(requireContext())
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getNotes().collectLatest {
                    if(it.isEmpty()) {
                        noData.visibility = View.VISIBLE
                    } else {
                        noData.visibility = View.GONE
                        adapters.asynclist.submitList(it)
                    }
                }
            }
        }

        addNoteFab.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_addFragment2)
        }
    }
}