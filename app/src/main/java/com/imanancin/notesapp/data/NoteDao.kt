package com.imanancin.notesapp.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Query("SELECT * FROM note")
    fun getAll(): Flow<List<Note>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg notes: Note)

    @Query("SELECT * from note WHERE uid = :uid LIMIT 1")
    fun getNote(uid: Int): Flow<Note>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateNote(note: Note) : Int

    @Query("DELETE from note WHERE uid = :uid")
    suspend fun delete(uid: Int)
}