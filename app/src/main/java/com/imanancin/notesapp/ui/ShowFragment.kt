package com.imanancin.notesapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.imanancin.notesapp.R
import com.imanancin.notesapp.ViewModelFactory
import com.imanancin.notesapp.domain.MyNote
import kotlinx.coroutines.launch


class ShowFragment : Fragment() {

    private val TAG = "ShowFragment"

    // bugs
//    val args: ShowFragmentArgs by navArgs()
    private val viewModel by activityViewModels<MyViewModel> { ViewModelFactory() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_show, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title = view.findViewById<EditText>(R.id.edTitle)
        val content = view.findViewById<EditText>(R.id.edContent)
        val noteId = arguments?.getInt("noteid")

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        (requireActivity() as MenuHost).addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.show_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                when(menuItem.itemId) {
                    R.id.save -> {
                        lifecycleScope.launch {
                            viewModel.updateNote(
                                MyNote(noteId, title.text.toString(), content.text.toString())
                            )
                        }
                        findNavController().popBackStack()
                    }
                    R.id.delete -> {
                        lifecycleScope.launch {
                            viewModel.deleteNote(noteId!!)
                        }
                        findNavController().popBackStack()
                    }
                }
                return true
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)


        lifecycleScope.launch {
            if (noteId != null) {
                viewModel.getNote(noteId).collect {
                    title.setText(it.title)
                    content.setText(it.content)
                }
            }
        }
    }
}